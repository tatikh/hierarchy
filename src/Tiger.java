public class Tiger extends Predator{
    private boolean line = true;
    private int lenghLine = 25;

    public boolean isLine() {
        return line;
    }

    public void setLine(boolean line) {
        this.line = line;
    }

    public int getLenghLine() {
        return lenghLine;
    }

    public void setLenghLine(int lenghLine) {
        this.lenghLine = lenghLine;
    }

    public Tiger(){
    }
    public Tiger(int lenghLine, double lenghLegs){
        setLenghLine(lenghLine);
        setLenghLegs(lenghLegs);
    }
    public Tiger(AnimColor color) {
        setAnimColor(color);
    }

    @Override
    protected String voice() {
        return "err err err";
    }
}
