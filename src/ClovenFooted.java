public class ClovenFooted {

    int numHoof = 4;
    private int numHorns = 2;

    public int getNumHoof() {
        return numHoof;
    }

    public void setNumHoof(int numHoof) {
        this.numHoof = numHoof;
    }

    public int getNumHorns() {
        return numHorns;
    }

    public void setNumHorns(int numHorns) {
        this.numHorns = numHorns;
    }

}
