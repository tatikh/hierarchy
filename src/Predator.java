public abstract class Predator extends Animal implements Running {

    private int numLegs = 4;
    private double lenghLegs;
    private int lenghWool = 20;

    public String run(){
        return "Predators are running";
    }

    public int getNumLegs() {
        return numLegs;
    }

    public void setNumLegs(int numLegs) {
        this.numLegs = numLegs;
    }

    public int getLenghWool() {
        return lenghWool;
    }

    public void setLenghWool(int lenghWool) {
        this.lenghWool = lenghWool;
    }

    public double getLenghLegs() {
        return lenghLegs;
    }

    public void setLenghLegs(double lenghLegs) {
        this.lenghLegs = lenghLegs;
    }
}
