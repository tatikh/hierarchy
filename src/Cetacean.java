public abstract class Cetacean extends Animal implements Swimming {
    int numFin = 4;

    public String voice(){
        return null;
    };

    public String swim(){
        return null;
    };

    public int getNumFin() {
        return numFin;
    }

    public void setNumFin(int numFin) {
        this.numFin = numFin;
    }
}