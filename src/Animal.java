public abstract class Animal {
    int heiht = 100;
    int weight = 20;
    int numTail = 1;
    double speed = 10.5;
    AnimColor animColor;

    public int getHeiht() {
        return heiht;
    }

    public void setHeiht(int heiht) {
        this.heiht = heiht;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public int getNumTail() {
        return numTail;
    }

    public void setNumTail(int numTail) {
        this.numTail = numTail;
    }

    public double getSpeed() {
        return speed;
    }

    public void setSpeed(double speed) {
        this.speed = speed;
    }

    public AnimColor getAnimColor() {
        return animColor;
    }

    public void setAnimColor(AnimColor animColor) {
        this.animColor = animColor;
    }

    protected abstract String voice();
}
