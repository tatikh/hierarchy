public class Lion extends Predator {
    private final boolean mane = true;
    private int lenghtManeWool;

    public int getLenghtManeWool() {
        return lenghtManeWool;
    }

    public void setLenghtManeWool(int lenghtManeWool) {
        this.lenghtManeWool = lenghtManeWool;
    }

    public Lion() {
    }
    public Lion(AnimColor color) {
        setAnimColor(color);
    }
    public Lion(AnimColor color, int lenghtManeWool) {
        setAnimColor(color);
        this.lenghtManeWool = lenghtManeWool;
    }

    public String run(){
        return "Lion is running";
    }
    public String run(double speed){
        setSpeed(speed);
            return "Lion is running with speed: " + speed;
    }

    @Override
    protected String voice() {
        return "rrrrrrr";
    }
}
